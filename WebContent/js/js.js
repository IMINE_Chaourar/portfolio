(function () {



    $(document).ready(function () {
        // Add smooth scrolling to all links
        $("a").on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });

    //add retation to laptop and code icons
    var retate = document.querySelectorAll('.laptop');

    retate.forEach(function (item) {
        var fitem = item.firstElementChild;

        item.addEventListener('mouseover', function () {
            fitem.classList.toggle('fa-pulse');
            fitem.classList.toggle('text-white');
        });


        item.addEventListener('mouseout', function () {
            fitem.classList.toggle('fa-pulse');
            fitem.classList.toggle('text-white');
        });

    });

    // make the nav bar sticky
    var scrollY = function () {
        var supportPageOffset = window.pageXOffset !== undefined;
        var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");

        var x = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft;
        var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;

        return y;
    }


    var nav = document.querySelector('.nav');
    var rect = nav.getBoundingClientRect();
    var top = rect.top + scrollY();

    var scrolling = function () {
        if (top < scrollY()) {
            nav.classList.add('nav-fixed');
        }
        else {
            nav.classList.remove('nav-fixed');
        }

        if (580 < scrollY()) {
            nav.classList.add('nav-color');
        } else {
            if (nav.classList.contains('nav-color')) {
                nav.classList.remove('nav-color');
            }
        }
    }

    window.addEventListener('scroll', scrolling);

    //hide menu
    // window.addEventListener('click', function () {
    //     setTimeout(function () {
    //         nav.classList.add('nav-hidden')
    //     }, 400)
    //     nav.classList.remove('nav-hidden')
    // })





})()